// load all the things we need
const LocalStrategy = require('passport-local').Strategy;

// load up the user model
const User = require('../models/user');

module.exports = function (passport) {

  // // used to serialize the user for the session
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });

  // LOCAL LOGIN =============================================================
  passport.use('local-login', new LocalStrategy({
      // by default, local strategy uses username and password, we will override with email
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true // allows to pass in the req from route (lets check if a user is logged in or not)
    },
    function (req, email, password, done) {
      if (validateEmail(email)) { // never trust incoming data
        if (email)
          email = email.toLowerCase();

        // asynchronous
        process.nextTick(function () {

          User.findOne({'local.email': email}, function (err, user) {
            // if there are any errors, return the error
            if (err)
              return done(err);

            // if no user is found, return the message
            if (!user)
              return done(null, false, req.flash('loginMessage', 'No user found.'));

            if (!user.validPassword(password))
              return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));

            // all is well, return user
            else
              return done(null, user);
          });
        });
      } else {
        return done(null, false, req.flash('loginMessage', 'Email is not valid!'));
      }
    }));

  // LOCAL REGISTER ============================================================
  passport.use('local-register', new LocalStrategy({
      // by default, local strategy uses username and password, we will override with email
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true // allows to pass in the req from route (lets check if a user is logged in or not)
    },
    function (req, email, password, done) {
      if (validateEmail(email)) {
        if (email)
          email = email.toLowerCase();
        // asynchronous
        process.nextTick(function () {
          // if the user is not already logged in:
          if (!req.user) {
            User.findOne({'local.email': email}, function (err, user) {
              // if there are any errors, return the error
              if (err)
                return done(err);
              // check to see if theres already a user with that email
              if (user) {
                return done(null, false, req.flash('registerMessage', 'Email is already taken.'));
              } else {
                // create the user
                var newUser = new User();
                var data = {email: email, password: password}; // could be more
                newUser = newUser.fillUser(newUser, data);
                newUser.local.password = newUser.generateHash(password);
                newUser.save(function (err) {
                  if (err)
                    return done(err);
                  return done(null, newUser);
                });
              }

            });
            // if the user is logged in but has no local account...
          } else {
            // user is logged in and already has a local account. Ignore registration. (You should log out before trying to create a new account, user!)
            return done(null, req.user);
          }
        });
      } else {
        // email not valid!
        return done(null, false, req.flash('registerMessage', 'That email is not valid!'));
      }
    }));
};

function validateEmail(mail) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(mail);
}
