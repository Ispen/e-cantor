const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const config = {
  'url': 'mongodb://db:25555/e-cantor',
  options: {
    useMongoClient: true,
    socketTimeoutMS: 0,
    keepAlive: true,
    reconnectTries: 30
  }
};

module.exports = function () {
  mongoose.connect(config.url, config.options).then(
    () => {
      console.log('Connected to db');
    },
    err => {
      console.error('Not connected to db!');
      console.error(err);
    });
  return mongoose;
}();
