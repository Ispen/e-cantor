const Cantor = require('../models/cantor');
const currencies = require('./currencies.json').items;
const Transaction = require('../models/transaction');

const DISCONNECT_TIME = 60000;

function prepareTransaction({data, user, prices, session}, done) {
  if (validateClientData(data)) {
    Cantor.findCantor( function (err, cantor) {
      if (err) {
        console.error(err);
        return done(err);
      } else {
        if (!cantor) {
          console.error('No cantor in database!');
          return done('Cantor not working, try again later.')
        } else {
          session.transaction = new Transaction(data, user.wallet, cantor.wallet, prices);
          if (session.transaction.isPossible) {
            return done(null, session.transaction.getConfirmation());
          } else {
            console.error('Transaction not possible!');
            return done('Transaction not possible!');
          }
        }
      }
    });
  } else {
    // wrong data from client
    console.error('Wrong data from client!');
    return done('Wrong data from client!');
  }
}

function make({transaction, user}, done) {
  // after parse in session, transaction obj are deprive of methods, but there is no need to regenerate them
  if (!transaction)
    return done('There is no transaction!', null);
  if (DISCONNECT_TIME < (Date.now() - Date.parse(global.currentPrices.publicationDate)))
    return done('Prices outdated! Disconnected!', null);
  if (!transaction.isPossible) // situation like 'not possible' shouldn't exist, but better to check that
    return done(transaction.error.message);
  Cantor.findCantor(function (err, cantor) {
    if (err) {
      console.error(err);
      return done('Cantor not working, try later.', null);
    } else {
      if (!cantor) {
        console.error('No cantor collection in DB!');
        return done('Cantor not working, try later.', null);
      } else {
        // TODO: recheck if cantor have money to trade
        cantor.wallet.forEach(function (item) {
          if (item.code === transaction.code) {
            item.amount += Math.floor((item.amount
              + transaction.change.cantor.currency)
              * Cantor.PRECISION) / Cantor.PRECISION;
          }
        });
        cantor.wallet[cantor.wallet.length - 1].amount =
          Math.floor((cantor.wallet[cantor.wallet.length - 1].amount
            + transaction.change.cantor.pln)
            * Cantor.PRECISION) / Cantor.PRECISION;
        user.wallet.forEach(function (item) {
          if (item.code === transaction.code) {
            item.amount = Math.floor((item.amount
            + transaction.change.user.currency)
            * 100) / 100;
          }
        });
        user.wallet[user.wallet.length - 1].amount =
          Math.floor((user.wallet[user.wallet.length - 1].amount
            + transaction.change.user.pln)
            * 100) / 100;
        user.save(function (err) {
          if (err)
            return done(err, null);
        });
        cantor.save(function (err) {
          if (err)
            return done(err, null);
        });
      }
    }
  });
  return done(null, 'Success!');
}

function validateClientData({action, amount, code}) {
  return (action === 'sell' || // check action
    action === 'buy') &&
    // check amount
    (!isNaN(parseFloat(amount)) &&
      (isFinite(amount)) &&
      (amount >= 0.0001) &&
      (amount <= 999999999999)) &&
    // check code (pick table of all supported codes and check if our is there)
      (currencies.map(function (item) {
        return item.code;
      }).indexOf(code) >= 0)
}

module.exports.make = make;
module.exports.prepareTransaction = prepareTransaction;

