const http = require('http');

const TIME_BETWEEN_PRICE_REQ = 1000 * 30; // 30 * 1000 = 30 sec

const options = {
  host: 'webtask.future-processing.com',
  path: '/currencies',
  port: '8068',
  method: 'GET',
  headers: {'accept': 'application/json'}
};

function reqPrices() {

  const req = http.request(options, callback);
  req.end();

  setTimeout(reqPrices, TIME_BETWEEN_PRICE_REQ);
}

callback = function (res) {
  let str = '';
  res.on('data', function (chunk) {
    str += chunk;
  });

  res.on('end', function () {
    global.currentPrices = JSON.parse(str);
  });
};

reqPrices(); // run service
