FROM node

RUN mkdir -p /e-cantor
WORKDIR /e-cantor
COPY package.json /e-cantor
RUN npm install

ADD . /e-cantor

EXPOSE 3000
CMD ["npm", "start"]
