const errList = [
  {id: 'email', msg: ''},
  {id: 'pass', msg: ''},
  {id: 'pass2', msg: ''}
];

function checkEmail() {
  const email = document.getElementById('email').value;
  if (validateEmail(email)) {
    handleError('email', '');
  } else {
    handleError('email', 'Please enter a valid email address');
  }
}

function checkPass() {
  const pass = document.getElementById('password').value;
  if (validatePassword(pass)) {
    handleError('pass', '');
  } else {
    handleError('pass', 'Min 8 chars, at least one number, small and big letter');
  }
}

function checkPass2() {
  // then check if they are the same
  const pass = document.getElementById('password').value;
  const pass2 = document.getElementById('password2').value;
  if (pass === pass2) {
    handleError('pass2', '');
  } else {
    handleError('pass2', 'Passwords do not match');
  }
}

function handleError(id, msg) {
  const errDOM = document.getElementById('errorBox');
  const buttonDOM = document.getElementById('submitButton');
  let errString = '';
  for (let i = 0; i < errList.length; i++) {
    if (errList[i].id === id)
      errList[i].msg = msg;
    if (errList[i].msg)
      errString += errList[i].msg + '\n';
  }
  if (errString) {
    // if there was errors
    errString = errString.slice(0, -1);
    errDOM.innerText = errString;
    if (errDOM.hasAttribute('hidden'))
      errDOM.removeAttribute('hidden');
    if (!buttonDOM.hasAttribute('disabled'))
      buttonDOM.setAttribute('disabled', '');
  } else {
    // if no errors
    if (!errDOM.hasAttribute('hidden'))
      errDOM.setAttribute('hidden', '');
    if (buttonDOM.hasAttribute('disabled'))
      buttonDOM.removeAttribute('disabled');
  }
}

function validatePassword(pass) {
  // length,>= 8, at least one number and at least one big letter
  let re = (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/);
  return re.test(pass);
}

function validateEmail(mail) {
  let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(mail);
}
