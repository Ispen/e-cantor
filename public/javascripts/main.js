const socket = io.connect(location.hostname + ':' + location.port);

let currentPrices = {}; // storing the latest prices info
let currentTransaction = {action: '', amount: '0', code: ''}; // what currency and sell or buy
const userBalance = new Map();
userBalance.update = function (wallet) {
  wallet.forEach(function (item) {
    userBalance.set(item.code, item.amount);
  });
};

$(document).ready(function () {
  /**
   * cookie parse
   */
  userBalance.update(getCookieWalletParse());
  /**
   * jQuery registers
   */
  $('#tradeInput').on('keyup keypress blur change', function (e) {
    currentTransaction.amount = e.currentTarget.value;
    $('#predictedPrice').data('updater')(e.currentTarget.value);
  });
  $('#predictedPrice').data('updater', function (value) {
    if (!value) {
      $('#predictedPrice').text('');
    } else {
      if (isNumeric(value)) {
        if (currentTransaction.action === 'buy') {
          $('#predictedPrice').text(buy(value));
        } else {
          $('#predictedPrice').text(sell(value));
        }
      }
    }
  });
  $('#priceForUnit').data('updater', function ({code, action}) {
    if (code) {
      let currency = getCurrencyData(code);
      if (action === 'buy') {
        $('#priceForUnit').text(currency.unit + ' ' + code + ' = ' + currency.sellPrice + ' PLN');
      } else {
        $('#priceForUnit').text(currency.unit + ' ' + code + ' = ' + currency.purchasePrice + ' PLN');
      }
    }
  });
  /**
   * with AJAX
   */
  $('#panelButton').click(function () {
    const data = currentTransaction;
    if (!data.amount) {
      // some error
    } else {
      $.ajax({
        type: 'POST',
        url: '/panel',
        data: data,
        success: function (res) {
          if (res.err) {
            console.error(res.err);
          } else {
            $('#confirmAmount').text('Amount: ' + res.confirmation.amount + ' ' + res.confirmation.code);
            $('#confirmPrice').text('Price: ' + res.confirmation.price);
            $('#newBalance').text('New Balance: ' + res.confirmation.newBalance + ' ' + 'PLN');
            $('#confirmationModal').modal();
          }
        }
      });
    }
  });
  $('#confirmationButton').click(function () {
    $.ajax({
      type: 'PUT',
      url: '/panel',
      success: function (res) {
        if (res.err) {
          console.log(res.err);
        } else {
          $('#confirmationModal').modal('toggle');
          $('#tradeModal').modal('toggle');
          location.reload();
        }
      }
    });
  });
  /**
   * modal
   */
  $("#tradeModal").on('hide.bs.modal', function () {
    clearAll();
  });
});
/**
 * Socket.io stuff
 */
socket.on('connect', function () {
  socket.emit('hello');
});

socket.on('disconnect', function () {
  disconnect();
});

socket.on('prices', function (data) {
  currentPrices = JSON.parse(data);
  reloader(currentPrices);
});
/**
 * other stuff
 */
function reloader(data) {
  // for bigger amount of data, better to use some kind of frontend framework
  $('#publicationDate').text(data.publicationDate);
  // reload prices in tables
  data.items.forEach(function (item) {
    $('#' + item.code + 'purchasePrice').text(item.purchasePrice);
    $('#' + item.code + 'sellPrice').text(item.sellPrice);
    (userBalance.get(item.amount)) ? //recalculate value only if user have money
      $('#' + item.code + 'value').text(
        Math.floor(item.purchasePrice / userBalance.get(item.code) / item.unit * 100) / 100)
      : false;
  });
  // trade
  $('#priceForUnit').data('updater')(currentTransaction);
}

function prepareTrade(code, action) {
  currentTransaction = {code: code, action: action}; //make it global
  if (currentTransaction.action === 'buy')
    $('#tradeLabel').text('How many ' + code + ' do you want to buy?');
  else
    $('#tradeLabel').text('How many ' + code + ' do you want to sell?');
  $('#priceForUnit').data('updater')(currentTransaction);
}

function clearAll() {
  $('#tradeInput').val('');
  $('#priceForUnit').html('');
  $('#predictedPrice').html('');
  currentTransaction.amount = 0;
}

function buy(buyAmount) {
  const balance = userBalance.get('PLN');
  const currencyToBuy = getCurrencyData(currentTransaction.code);
  let calculatedPrice = calculatePrice(buyAmount, currencyToBuy.sellPrice, currencyToBuy.unit);
  if (calculatedPrice === -1) {
    return 'Wrong values!';
  } else {
    calculatedPrice = Math.ceil(calculatedPrice * 100) / 100; // round always up
    if (calculatedPrice > balance) {
      return 'You haven\'t that amount of money! You need to pay ' + calculatedPrice + ' PLN';
    } else {
      return 'You need to pay: ' + calculatedPrice + ' PLN';
    }
  }
}

function sell(sellAmount) {
  const maxAmountToSell = userBalance.get(currentTransaction.code);
  const currencyToSell = getCurrencyData(currentTransaction.code);
  let predictedPrice = calculatePrice(sellAmount, currencyToSell.purchasePrice, currencyToSell.unit);
  if (predictedPrice === -1) {
    return 'Wrong values!';
  } else if (sellAmount <= maxAmountToSell) {
    return 'You receive: ' + predictedPrice + ' PLN';
  } else
    return 'You haven\'t that amount of money! You could receive: ' + predictedPrice + ' PLN';
}

function getCookieWalletParse() {
// not universal, just for user wallet param - cookie contains only that
  return JSON.parse(document.cookie.slice(7));
}

function getCurrencyData(code) {
  return currentPrices.items.filter(function (item) {
    return item.code === code;
  }).pop();
}

function disconnect() {
  window.location.pathname = '/logout';
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function calculatePrice(amount, price, unit = 1) {
  if (isNumeric(amount) &&
    isNumeric(price) &&
    isNumeric(unit) &&
    (amount >= 0.0001) &&
    (amount <= 999999999999)) {
    let calculatedPrice = amount * price / unit;
    return Math.floor(calculatedPrice * 100) / 100; // round always down
  } else return -1;
}
