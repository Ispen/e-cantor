# DESCRIPTION #

E-cantor is the simple exchange application in node.js with mongodb, for recruitment purposes.  

### How do I get set up? ###
For docker version, all you need, is to clone docker branch and run docker-compose:
```
git clone https://bitbucket.org/Ispen/e-cantor.git --branch docker
```
Get inside e-cantor and type:
```
docker-compose up
```
The page is running on the: [https://localhost:3000](https://localhost:3000)

### Progress ###
Registration/Login panel - DONE  
Database access - DONE  
Exchange panel - DONE  
Profile editor - in progress

