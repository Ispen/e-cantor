const express = require('express');
const passport = require('passport');
const router = express.Router();

router.get('/', function (req, res, next) {
  res.render('login', {message: req.flash('loginMessage')});
});

router.post('/', passport.authenticate('local-login', {
  successRedirect: '/panel', // redirect to the panel section
  failureRedirect: '/login', // redirect back to the login page if there is an error
  failureFlash: true // allow flash messages
}));

module.exports = router;
