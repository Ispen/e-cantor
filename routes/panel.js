const express = require('express');
const router = express.Router();

const exchange = require('../config/exchange');

router.get('/', isLoggedIn, socketIO, function (req, res, next) {
  // prepare some data to render
  const wallet = req.user.wallet;
  const items = global.currentPrices.items;
  for (let i = 0; i < wallet.length - 1; i++) { // last one is always PLN - ignore it
    items[i].amount = wallet[i].amount; // adding field with value from db
    items[i].value = Math.floor(items[i].purchasePrice * items[i].amount / items[i].unit * 100) / 100; // adding field with calculated price
  }
  let balance = wallet[wallet.length - 1]; // copy last element witch is PLN
  balance = balance.amount; // need to do that separatly
  const text = JSON.stringify(wallet);

  res.cookie('wallet', text, {encode: String});
  res.render('panel', {
    user: req.user,
    items: items,
    balance: balance
  });
});

router.post('/', isLoggedIn, function (req, res, next) {
  // prepare data for controler
  exchange.prepareTransaction({
    data: req.body, // user input
    user: req.user, // user obj from db
    prices: global.currentPrices, // current prices
    session: req.session // session to store transaction
  }, function (err, confirmation) {
    res.json({err, confirmation});
  });
});

router.put('/', isLoggedIn, function (req, res, next) {
  // prepare data for controler
  exchange.make({
    transaction: req.session.transaction,
    user: req.user
  }, function (err, confirmation) {
    res.json({err, confirmation});
  });
});

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated())
    return next();

  res.redirect('/login');
}

function socketIO(req, res, next) {
  const io = req.app.get('io');
  io.on('connection', function (client) {
    client.join('default'); // join to room
    client.emit('prices', JSON.stringify(global.currentPrices));
  });
  return next();
}

module.exports = router;
