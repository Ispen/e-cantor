const express = require('express');
const passport = require('passport');
const router = express.Router();

router.get('/', function (req, res, next) {
  res.render('register', {message: req.flash('registerMessage')});
});

router.post('/', passport.authenticate('local-register', {
  successRedirect: '/panel', // redirect to the secure profile section
  failureRedirect: '/register', // redirect back to the register page if there is an error
  failureFlash: true // allow flash messages
}));

module.exports = router;
