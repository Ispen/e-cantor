/**
 * @return {boolean}
 */
function Transaction({action, amount, code}, userWallet, cantor, prices) {
  this.amount = parseAmount(amount);
  this.confirmation = {
    action: action,
    amount: amount,
    cost: 0,
    newBalance: userWallet[userWallet.length - 1].amount, // later add cost to this
    code: code,
    price: 0,
    value: 0
  };
  this.code = code;
  this.currency = getItemByCode(prices.items, code);
  this.current = {
    cantorBalance: {
      currency: getItemByCode(cantor, code).amount,
      pln: getItemByCode(cantor, 'PLN').amount
    },
    userBalance: {
      currency: getItemByCode(userWallet, code).amount,
      pln: getItemByCode(userWallet, 'PLN').amount
    }
  };

  this.change = function (t) {
    let schema = {
      cantor: {
        currency: 0,
        pln: 0
      },
      user: {
        currency: 0,
        pln: 0
      }
    };
    if (action === 'buy') {
      schema.cantor.currency = -t.amount;
      schema.cantor.pln = buy(t.amount, t.currency.sellPrice, t.currency.unit);
      schema.user.currency = t.amount;
      schema.user.pln = -schema.cantor.pln;
    } else {
      schema.cantor.currency = t.amount;
      schema.cantor.pln = -sell(t.amount, t.currency.sellPrice, t.currency.unit);
      schema.user.currency = -t.amount;
      schema.user.pln = -schema.cantor.pln;
    }
    return schema;
  }(this);

  this.error = {message: ''};

  this.isPossible = function (t) {
    let cantorB = t.current.cantorBalance;
    let userB = t.current.userBalance;
    if (action === 'buy') {
      // is the cantor have enough money?
      if (cantorB.currency >= t.amount) {
        t.confirmation.value = t.amount;
        t.confirmation.cost = buy(t.amount, t.currency.sellPrice, t.currency.unit);
        t.confirmation.price = t.currency.sellPrice;
        t.confirmation.newBalance = Math.floor((t.confirmation.newBalance - t.confirmation.cost) * 100) / 100;
        // do user have enough money?
        if (userB.pln >= t.confirmation.cost) {
          return true;
        } else {
          t.error.message = 'You haven\'t enough money!';
          return false;
        }
      } else {
        t.error.message = 'Cantor can\'t complete the transaction.';
        return false;
      }
    } else {
      // do user have enough money to sell?
      if (userB.currency >= t.amount) {
        t.confirmation.cost = t.amount;
        t.confirmation.value = sell(t.amount, t.currency.purchasePrice, t.currency.unit);
        t.confirmation.price = t.currency.purchasePrice;
        t.confirmation.newBalance = Math.floor((t.confirmation.newBalance + t.confirmation.value) * 100) / 100;
        // is the cantor have enough money to buy?
        if (cantorB.pln >= t.confirmation.value) {
          return true;
        } else {
          t.error.message = 'Cantor can\'t complete the transaction.';
          return false;
        }
      } else {
        t.error.message = 'You haven\'t enough money!';
      }
    }
    t.error.message = 'Somting goes wrong!';
    return false;
  }(this);

  this.getConfirmation = function () {
    return this.confirmation;
  };
}

function buy(a, p, u) {
  let calcPrice = calculatePrice(a, p, u);
  return Math.round((calcPrice + 0.005) * 100) / 100; // round always up
}

function sell(a, p, u) {
  return calculatePrice(a, p, u);
}

function calculatePrice(amount, price, unit = 1) {
  let calculatedPrice = amount * price / unit;
  return Math.floor(calculatedPrice * 100) / 100;
}

function getItemByCode(array, code) {
  return array.filter(function (item) {
    return item.code === code;
  }).pop();
}

function parseAmount(a) {
  a = parseFloat(a);
  if (!isNaN(a) &&
    (isFinite(a)) &&
    (a >= 0.0001) &&
    (a <= 999999999999)) {
    return a;
  } else {
    return undefined;
  }
}

module.exports = Transaction;
