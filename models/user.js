const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');

const items = require('../config/currencies.json').items;

const userSchema = mongoose.Schema({
  local: {
    email: String,
    password: String
  },
  wallet: [{code: String, amount: Number}],
  personalData: {
    firstName: String,
    lastName: String,
    street: String,
    city: String,
    postalCode: String
  }}, {
  timestamps: true
});

userSchema.methods.fillUser = function (user, data) {
  user.local.email = data.email;
  user.local.password = userSchema.methods.generateHash(data.password);

  let tab = []; // last one always for PLN
  for (let i = 0; i < items.length; i++) {
    tab.push({code: items[i].code, amount: 0});
  }
  tab.push({code: 'PLN', amount: 10000}); // 10k FOR DEBUG ONLY
  user.wallet = tab;

  user.personalData.firstName = data.firstName || '';
  user.personalData.lastName = data.lastName || '';
  user.personalData.street = data.street || '';
  user.personalData.city = data.city || '';
  user.personalData.postalCode = data.postalCode || '';

  return user;
};
// generating a hash
userSchema.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
// checking if password is valid
userSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.local.password);
};
// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
