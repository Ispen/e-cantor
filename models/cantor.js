const mongoose = require('mongoose');
const CANTOR_BRANCH_NAME = 'cantor1';

const cantorSchema = mongoose.Schema({
  branch: String,
  wallet: [{code: String, amount: Number}]
}, {collection: 'cantors'});

cantorSchema.statics.findCantor = function (cb) {
  return this.findOne({
    branch: CANTOR_BRANCH_NAME
  }, cb);
};

// cantorSchema.methods.canTrade = function (code, amount) { // TODO: move the checking stuff from transaction controller to model
//   return true;
//   return false;
// };

cantorSchema.statics.BRANCH_NAME = 'cantor1';
cantorSchema.statics.PRECISION = 100000; // 10 = 0.1; 1000 = 0.001, etc.

module.exports = mongoose.model('cantor', cantorSchema);
