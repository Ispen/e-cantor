const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const passport = require('passport');
const session = require('express-session');

const app = express();
require('./config/passport')(passport);

const mongoose = require('./config/database');

const index = require('./routes/index');
const login = require('./routes/login');
const logout = require('./routes/logout');
const panel = require('./routes/panel');
const register = require('./routes/register');

app.set('trust proxy', 1); // trust first proxy
/* view engine setup */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(session({
  secret: 'SjamYuJWxT9zudaH6XtXlVapkFi5PxjdssZEOm8MzXaBWEttzjl3knvBgq3AqBIv',
  resave: true, // overwrite even no changes
  saveUninitialized: true, // create session even nothing stored
  cookie: {
    httpOnly: true,
    secure: true
  }
}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

app.use('/', index);
app.use('/login', login);
app.use('/logout', logout);
app.use('/panel', panel);
app.use('/register', register);

function updatePrice() {
  const io = app.get('io');
  (global.currentPrices) ? io.to('default').emit('prices', JSON.stringify(global.currentPrices))
    : false;
  setTimeout(updatePrice, 60 * 1000);
}
updatePrice();

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
  console.error(err.message);
});

process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log('Mongoose disconnected on app termination');
    process.exit(0);
  });
});

// process.on('uncaughtException', function (err) {
//   console.error('Can\'t connect to server, check your internet connection!');
// });

module.exports = app;
